# pfUI Elite Overlay

An extension for pfUI which adds dragon textures to elite, rare and worldbosses.

![screenshot](https://raw.githubusercontent.com/shagu/ShaguAddons/master/_img/pfUI-eliteoverlay/screenshot.png)

## Installation
1. Download **[Latest Version](https://gitlab.com/shagu/pfUI-eliteoverlay/-/archive/master/pfUI-eliteoverlay-master.zip)**
2. Unpack the Zip file
3. Rename the folder "pfUI-eliteoverlay-master" to "pfUI-eliteoverlay"
4. Copy "pfUI-eliteoverlay" into Wow-Directory\Interface\AddOns
5. Restart Wow

## Credits
Artwork by @eNtreDefias
